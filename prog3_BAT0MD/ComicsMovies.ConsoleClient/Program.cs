﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ComicsMovies.ConsoleClient
{
    public class Director
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Nationality { get; set; }
        public string Birth { get; set; }
        public int Salary { get; set; }
        public override string ToString()
        {
            return $"ID = {Id}\tFirst Name={FirstName}\tLast Name={LastName}\tNationality={Nationality}\tBirth={Birth}\tSalary={Salary}";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("WAITING...");
            Console.ReadLine();

            string url = "http://localhost:55723/api/DirectorsApi/";

            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Director>>(json);
                foreach (var item in list)
                {
                    Console.WriteLine(item);
                }
                Console.ReadLine();

                Dictionary<string, string> postData;
                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(Director.FirstName), "Jim");
                postData.Add(nameof(Director.LastName), "Abrahams");
                postData.Add(nameof(Director.Nationality), "amerikai");
                postData.Add(nameof(Director.Salary), "5352523");
                postData.Add(nameof(Director.Birth), "1944.05.10");
                response = client.PostAsync(url + "add", new FormUrlEncodedContent
                    (postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ADD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                int dirId = JsonConvert.DeserializeObject<List<Director>>(json).Single(x => x.LastName == "Abrahams" && x.FirstName == "Jim").Id;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(Director.Id), dirId.ToString());
                postData.Add(nameof(Director.FirstName), "Jim");
                postData.Add(nameof(Director.LastName), "Abrahams");
                postData.Add(nameof(Director.Nationality), "amerikai");
                postData.Add(nameof(Director.Salary), "1000000");
                postData.Add(nameof(Director.Birth), "1944.05.10");
                response = client.PostAsync(url + "mod", new FormUrlEncodedContent
                    (postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                response = client.GetStringAsync(url + "del/" + dirId).Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("DEL: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();
            }
        }
    }
}
