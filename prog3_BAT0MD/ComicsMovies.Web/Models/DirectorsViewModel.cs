﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ComicsMovies.Web.Models
{
    public class DirectorsViewModel
    {
        public Director EditedDirector { get; set; }
        public List<Director> ListOfDirectors { get; set; }
    }
}