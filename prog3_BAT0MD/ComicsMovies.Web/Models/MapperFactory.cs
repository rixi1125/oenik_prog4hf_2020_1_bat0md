﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ComicsMovies.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ComicsMovies.Data.rendezo, ComicsMovies.Web.Models.Director>()
                .ForMember(dest => dest.Id, map => map.MapFrom(src => src.rendezoid))
                .ForMember(dest => dest.LastName, map => map.MapFrom(src => src.vezeteknev))
                .ForMember(dest => dest.FirstName, map => map.MapFrom(src => src.keresztnev))
                .ForMember(dest => dest.Nationality, map => map.MapFrom(src => src.nemzetiseg))
                .ForMember(dest => dest.Birth, map => map.MapFrom(src => src.sz_datum))
                .ForMember(dest => dest.Salary, map => map.MapFrom(src => src.fizetes));
            });

            return config.CreateMapper();
        }
    }
}