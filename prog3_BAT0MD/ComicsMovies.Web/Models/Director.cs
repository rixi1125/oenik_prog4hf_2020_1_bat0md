﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ComicsMovies.Web.Models
{
    public class Director
    {
        [Display(Name = "Director ID")]
        [Required]
        public int Id { get; set; }

        [Display(Name = "First Name")]
        [Required]
        [StringLength(30, MinimumLength = 5)]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [Required]
        [StringLength(30, MinimumLength = 5)]
        public string LastName { get; set; }

        [Display(Name = "Nationality")]
        [Required]
        public string Nationality { get; set; }

        [Display(Name = "Birth")]
        [Required]
        public string Birth { get; set; }

        [Display(Name = "Salary")]
        [Required]
        public int Salary { get; set; }
    }
}