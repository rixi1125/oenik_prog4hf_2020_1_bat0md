﻿using AutoMapper;
using ComicsMovies.Data;
using ComicsMovies.Logic;
using ComicsMovies.Repository;
using ComicsMovies.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ComicsMovies.Web.Controllers
{
    public class DirectorsApiController : ApiController
    {
        public class ApiResult
        {
            public bool OperationResult { get; set; }
        }

        ILogic logic;
        IMapper mapper;

        public DirectorsApiController()
        {
            ComicsMoviesDatabaseEntities cmde = new ComicsMoviesDatabaseEntities();
            DirectorRepository dirRepo = new DirectorRepository(cmde);
            logic = new ComicsLogic(dirRepo);
            mapper = MapperFactory.CreateMapper();
        }

        // GET api/DirectorsApi
        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Director> GetAll()
        {
            var directors = logic.GetAllDirectors();
            return mapper.Map<IList<Data.rendezo>, List<Models.Director>>(directors);
        }

        // GET api/DirectorsApi/del/42
        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneDirector(int id)
        {
            bool success = logic.Remove_Director(id);
            return new ApiResult() { OperationResult = success };
        }

        // POST api/DirectorsApi/add
        [ActionName("add")]
        [HttpPost]
        public ApiResult AddOneDirector(Director dir)
        {
            logic.Add_Director(dir.FirstName, dir.LastName, dir.Nationality, dir.Birth, dir.Salary);
            return new ApiResult() { OperationResult = true };
        }

        // POST api/DirectorsApi/mod + director
        [ActionName("mod")]
        [HttpPost]
        public ApiResult ModOneDirector(Director dir)
        {
            bool success = logic.ChangeDirector(dir.Id, dir.FirstName, dir.LastName, dir.Birth, dir.Nationality, dir.Salary);
            return new ApiResult() { OperationResult = success };
        }
    }
}
