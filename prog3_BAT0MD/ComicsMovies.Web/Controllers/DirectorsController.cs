﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ComicsMovies.Logic;
using ComicsMovies.Web.Models;
using AutoMapper;
using ComicsMovies.Repository;
using ComicsMovies.Data;

namespace ComicsMovies.Web.Controllers
{
    public class DirectorsController : Controller
    {
        ILogic logic;
        IMapper mapper;
        DirectorsViewModel vm;

        public DirectorsController()
        {
            ComicsMoviesDatabaseEntities cmde = new ComicsMoviesDatabaseEntities();
            DirectorRepository dirRepo = new DirectorRepository(cmde);
            logic = new ComicsLogic(dirRepo);
            mapper = MapperFactory.CreateMapper();

            vm = new DirectorsViewModel();
            vm.EditedDirector = new Director();
            var directors = logic.GetAllDirectors();
            vm.ListOfDirectors = mapper.Map<IList<Data.rendezo>, List<Models.Director>>(directors);
        }

        private Director GetDirectorModel(int id)
        {
            rendezo oneDirector = logic.GetOneDirector(id);
            return mapper.Map<Data.rendezo, Models.Director>(oneDirector);
        }

        // GET: Directors
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("DirectorIndex", vm);
        }

        // GET: Directors/Details/5
        public ActionResult Details(int id)
        {
            return View("DirectorDetails", GetDirectorModel(id));
        }

        public ActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete fail";
            if (logic.Remove_Director(id)) TempData["editResult"] = "Delete OK";
            return RedirectToAction(nameof(Index));
        }

        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedDirector = GetDirectorModel(id);
            return View("DirectorIndex", vm);
        }

        [HttpPost]
        public ActionResult Edit(Director dir, string editAction)
        {
            if(ModelState.IsValid && dir != null)
            {
                TempData["editResult"] = "Edit OK";
                if(editAction == "AddNew")
                {
                    logic.Add_Director(dir.FirstName, dir.LastName, dir.Nationality, dir.Birth, dir.Salary);
                }
                else
                {
                    bool success = logic.ChangeDirector(dir.Id,dir.FirstName,dir.LastName,dir.Birth,dir.Nationality,dir.Salary);
                    if (!success) TempData["editResult"] = "EDIT fail";
                }
                return RedirectToAction(nameof(Index));
            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.EditedDirector = dir;
                return View("DirectorIndex", vm);
            }
        }
    }
}
