﻿// <copyright file="IActorRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ComicsMovies.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ComicsMovies.Data;

    /// <summary>
    /// Interface for actors.
    /// </summary>
    public interface IActorRepository : IRepository<szinesz>
    {
        /// <summary>
        /// Delete method.
        /// </summary>
        /// <param name="name">the actor's name.</param>
        void DeleteActor(string name);

        /// <summary>
        /// Update method.
        /// </summary>
        /// <param name="name">name.</param>
        /// <param name="newName">newName.</param>
        void UpdateActorLastName(string name, string newName);

        /// <summary>
        /// GetOne method.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>an actor.</returns>
        szinesz GetOne(int id);

        /// <summary>
        /// ListActorsWithNames method.
        /// </summary>
        /// <returns>string list of actor names.</returns>
        List<string> ListActorsWithNames();

        /// <summary>
        /// IsInDatabase check method.
        /// </summary>
        /// <param name="name">name.</param>
        /// <returns>true or false.</returns>
        bool IsInDatabase(string name);
    }
}
