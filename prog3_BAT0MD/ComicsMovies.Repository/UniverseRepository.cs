﻿// <copyright file="UniverseRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ComicsMovies.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ComicsMovies.Data;

    /// <summary>
    /// Universe repo.
    /// </summary>
    public class UniverseRepository
    {
        private readonly ComicsMoviesDatabaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="UniverseRepository"/> class.
        /// The universe repo.
        /// </summary>
        public UniverseRepository()
        {
            this.db = new ComicsMoviesDatabaseEntities();
        }

        /// <summary>
        /// Lists all of the universe entities.
        /// </summary>
        /// <returns>list of universes.</returns>
        public List<univerzum> GetAll()
        {
            return this.db.univerzum.ToList();
        }
    }
}
