﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ComicsMovies.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ComicsMovies.Data;

    /// <summary>
    /// Interface.
    /// </summary>
    /// <typeparam name="T">T.</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Add method.
        /// </summary>
        /// <param name="entity">entity.</param>
        void Add(T entity);

        /// <summary>
        /// GetAll method.
        /// </summary>
        /// <returns>List of entities.</returns>
        List<T> GetAll();
    }
}
