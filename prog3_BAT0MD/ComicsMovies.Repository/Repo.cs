﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComicsMovies.Repository
{
    public abstract class Repo<T> : IRepo<T> where T : class
    {
        protected DbContext ctx;
        public Repo(DbContext ctx)
        {
            this.ctx = ctx;
        }
        public IQueryable<T> GetAll()
        {
            return ctx.Set<T>(); // "Set" as a noun, not as a verb!!!
        }

        public abstract T GetOne(int id);

        public void Insert(T entity)
        {
            ctx.Set<T>().Add(entity);
            ctx.SaveChanges();
        }

        public bool Remove(int id)
        {
            T entity = GetOne(id);
            if (entity == null) return false;
            ctx.Set<T>().Remove(entity);
            ctx.SaveChanges();
            return true;
        }
    }
}
