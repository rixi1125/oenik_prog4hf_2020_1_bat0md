﻿// <copyright file="ActorRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ComicsMovies.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ComicsMovies.Data;

    /// <summary>
    /// Actor repo.
    /// </summary>
    public class ActorRepository : IActorRepository
    {
        private readonly ComicsMoviesDatabaseEntities db;

        private List<string> names = new List<string>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ActorRepository"/> class.
        /// The constructor.
        /// </summary>
        public ActorRepository()
        {
            this.db = new ComicsMoviesDatabaseEntities();
        }

        /// <summary>
        /// Delete an actor with the given name.
        /// </summary>
        /// <param name="name">actor's name.</param>
        public void DeleteActor(string name)
        {
            if (this.IsInDatabase(name))
            {
                var roles = from x in this.db.szinesz
                            join y in this.db.szereples
                            on x.szineszid equals y.szineszid
                            where x.keresztnev + " " + x.vezeteknev == name
                            select y;

                foreach (var item in roles)
                {
                    this.db.szereples.Remove(item);
                }

                var delete = this.db.szinesz.Single(x => x.keresztnev + " " + x.vezeteknev == name);
                this.db.szinesz.Remove(delete);
                this.db.SaveChanges();
            }
            else
            {
                throw new GivenEntityNotInDatabaseException("This Actor is not in the database");
            }
        }

        /// <summary>
        /// Lists all of the actor entities.
        /// </summary>
        /// <returns>All of the actors.</returns>
        public List<szinesz> GetAll()
        {
            return this.db.szinesz.ToList();
        }

        /// <summary>
        /// Lists actors with names.
        /// </summary>
        /// <returns>list of movie titles.</returns>
        public List<string> ListActorsWithNames()
        {
            List<string> actors = new List<string>();

            foreach (var item in this.db.szinesz)
            {
                actors.Add(item.keresztnev + " " + item.vezeteknev);
            }

            return actors;
        }

        /// <summary>
        /// Updates the actor's last name.
        /// </summary>
        /// <param name="name">name.</param>
        /// <param name="newName">newName.</param>
        public void UpdateActorLastName(string name, string newName)
        {
            if (this.IsInDatabase(name))
            {
                var actor = this.db.szinesz.Single(x => x.keresztnev + " " + x.vezeteknev == name);
                actor.vezeteknev = newName;
                this.db.SaveChanges();
            }
            else
            {
                throw new Exception("The Actor is not in the database!");
            }
        }

        /// <summary>
        /// Add a new actor to the database.
        /// </summary>
        /// <param name="entity">the actor entity.</param>
        public void Add(szinesz entity)
        {
            var temp = from x in this.db.szinesz
                       orderby x.szineszid descending
                       select new
                       {
                           x.szineszid,
                       };

            var lastID = temp.First().szineszid;
            entity.szineszid = lastID + 1;

            szinesz actor = new szinesz()
            {
                keresztnev = entity.keresztnev,
                vezeteknev = entity.vezeteknev,
                szemszin = entity.szemszin,
                nemzetiseg = entity.nemzetiseg,
                sz_datum = entity.sz_datum,
            };

            this.names.Add(entity.keresztnev + " " + entity.vezeteknev);
            this.db.szinesz.Add(actor);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Check if the actor already in the database or not.
        /// </summary>
        /// <param name="name">the name of the circuit.</param>
        /// <returns>returns true if the actor is in the database, false if not.</returns>
        public bool IsInDatabase(string name)
        {
            foreach (var item in this.db.szinesz)
            {
                this.names.Add(item.keresztnev + " " + item.vezeteknev);
            }

            return this.names.Contains(name);
        }

        /// <summary>
        /// Gives back an actor with the specified id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>an actor.</returns>
        public szinesz GetOne(int id)
        {
            return this.GetAll().Where(x => x.szineszid == id).FirstOrDefault();
        }
    }
}
