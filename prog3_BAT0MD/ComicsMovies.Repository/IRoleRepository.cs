﻿// <copyright file="IRoleRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ComicsMovies.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using ComicsMovies.Data;

    /// <summary>
    /// Interface for roles.
    /// </summary>
    public interface IRoleRepository : IRepository<szereples>
    {
        /// <summary>
        /// Delete method.
        /// </summary>
        /// <param name="title">title.</param>
        /// <param name="actorname">actorname.</param>
        void DeleteRole(string title, string actorname);

        /// <summary>
        /// GetOne method.
        /// </summary>
        /// <param name="movieid">movieid.</param>
        /// <param name="actorid">actorid.</param>
        /// <returns>a role.</returns>
        szereples GetOne(int movieid, int actorid);

        /// <summary>
        /// ListRoleEntitiesWithNames method.
        /// </summary>
        /// <returns>list of role entities with names.</returns>
        List<string> ListRoleEntitiesWithNames();
    }
}
