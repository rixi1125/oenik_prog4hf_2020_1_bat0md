﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ComicsMovies.Wpf
{
    class MainVM : ViewModelBase
    {
        private MainLogic logic;
        private DirectorVM selectedDirector;
        private ObservableCollection<DirectorVM> allDirectors;

        public ObservableCollection<DirectorVM> AllDirectors
        {
            get { return allDirectors; }
            set { Set(ref allDirectors, value); }
        }

        public DirectorVM SelectedDirector
        {
            get { return selectedDirector; }
            set { Set(ref selectedDirector, value); }
        }

        public ICommand AddCmd { get; private set; }
        public ICommand DelCmd { get; private set; }
        public ICommand ModCmd { get; private set; }
        public ICommand LoadCmd { get; private set; }

        public Func<DirectorVM, bool> EditorFunc { get; set; }

        public MainVM()
        {
            logic = new MainLogic();

            DelCmd = new RelayCommand(() => logic.ApiDelDirector(selectedDirector));
            AddCmd = new RelayCommand(() => logic.EditDirector(null, EditorFunc));
            ModCmd = new RelayCommand(() => logic.EditDirector(selectedDirector, EditorFunc));
            LoadCmd = new RelayCommand(() => 
                AllDirectors = new ObservableCollection<DirectorVM>(logic.ApiGetDirectors()));
        }
    }
}
