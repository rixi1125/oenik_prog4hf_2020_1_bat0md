﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ComicsMovies.Wpf
{
    class MainLogic
    {
        string url = "http://localhost:55723/api/DirectorsApi/";
        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "DirectorResult");
        }

        public List<DirectorVM> ApiGetDirectors()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<DirectorVM>>(json);
            return list;
        }

        public void ApiDelDirector(DirectorVM director)
        {
            bool success = true;
            if (director != null)
            {
                string json = client.GetStringAsync(url + "del/" + director.Id.ToString()).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }

        bool ApiEditDirector(DirectorVM dir, bool isEditing)
        {
            if(dir == null)
            {
                return false;
            }

            string myUrl = isEditing ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();

            if(isEditing)
                postData.Add(nameof(DirectorVM.Id), dir.Id.ToString());
                postData.Add(nameof(DirectorVM.FirstName), dir.FirstName);
                postData.Add(nameof(DirectorVM.LastName), dir.LastName);
                postData.Add(nameof(DirectorVM.Nationality), dir.Nationality);
                postData.Add(nameof(DirectorVM.Birth), dir.Birth);
                postData.Add(nameof(DirectorVM.Salary), dir.Salary.ToString());
                

            string json = client.PostAsync(myUrl, new FormUrlEncodedContent
                (postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }

        public void EditDirector(DirectorVM dir, Func<DirectorVM, bool> editor)
        {
            DirectorVM clone = new DirectorVM();
            if(dir != null)
            {
                clone.CopyFrom(dir);
            }
            bool? success = editor?.Invoke(clone);
            if(success == true)
            {
                if (dir != null)
                    success = ApiEditDirector(clone, true);
                else
                   success = ApiEditDirector(clone, false);
            }
            SendMessage(success == true);
        }
    }
}
