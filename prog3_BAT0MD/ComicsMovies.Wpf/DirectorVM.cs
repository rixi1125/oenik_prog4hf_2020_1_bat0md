﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComicsMovies.Wpf
{
    class DirectorVM : ObservableObject
    {
        private int id;
        private string first_name;
        private string last_name;
        private int salary;
        private string nationality;
        private string birth;

        public int Id
        {
            get { return id; }
            set { Set(ref id, value); }
        }
        public string FirstName
        {
            get { return first_name; }
            set { Set(ref first_name, value); }
        }

        public string LastName
        {
            get { return last_name; }
            set { Set(ref last_name, value); }
        }

        public string Nationality
        {
            get { return nationality; }
            set { Set(ref nationality, value); }
        }

        public string Birth
        {
            get { return birth; }
            set { Set(ref birth, value); }
        }

        public int Salary
        {
            get { return salary; }
            set { Set(ref salary, value); }
        }
        
        public void CopyFrom(DirectorVM other)
        {
            if(other == null)
            {
                return;
            }

            this.Id = other.Id;
            this.FirstName = other.FirstName;
            this.LastName = other.LastName;
            this.Nationality = other.Nationality;
            this.Birth = other.Birth;
            this.Salary = other.Salary;
        }

    }
}
